/**
 * Class ClassRoom class provides for project.
 * 
 * @author <A HREF="mailto:phucdd@greenglobal.vn">[Phuc]</A>
 * @version $Revision: 1.0.0.0 $ $Date: 2019/05/23 $
 * @see [ClassRoom]
 */
package crudsample.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The ClassRoom class provides for project
 * @author DAOPHUC
 *
 */
@Entity
@Table(name = "class_room")
public class ClassRoom implements Serializable {

	/**
	 * SerialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Id
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	/**
	 * Name
	 */
	@Column(name = "name")
	private String name;
	
	/**
	 * Teacher
	 */
	@Column(name = "teacher")
	private String teacher;
	
	/**
	 * Major
	 */
	@Column(name = "major")
	private String major;

	/**
	 * Get id of a class room
	 * @return Id
	 */
	public long getId() {
		return id;
	}
	
	/**
	 * Get name of a class room
	 * @return The name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set name of a class room
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Get teacher of a class room
	 * @return The teacher
	 */
	public String getTeacher() {
		return teacher;
	}

	/**
	 * Set teacher of a class room
	 * @param teacher
	 */
	public void setTeacher(String teacher) {
		this.teacher = teacher;
	}

	/**
	 * Get major of a class room
	 * @return The major
	 */
	public String getMajor() {
		return major;
	}

	/**
	 * Set major of a class room
	 * @param major
	 */
	public void setMajor(String major) {
		this.major = major;
	}
}
