/**
 * Class Student class provides for project.
 * 
 * @author <A HREF="mailto:phucdd@greenglobal.vn">[Phuc]</A>
 * @version $Revision: 1.0.0.0 $ $Date: 2019/05/23 $
 * @see [Student]
 */
package crudsample.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * The Student class provides for project
 * @author DAOPHUC
 *
 */
@Entity
@Table(name = "student")
public class Student implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Id
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	/**
	 * Name
	 */
	@Column(name = "name")
	private String name;
	
	/**
	 * Email
	 */
	@Column(name = "email")
	private String email;
	
	/**
	 * Phone
	 */
	@Column(name = "phone")
	private String phone;
	
	/**
	 * Password
	 */
	@Column(name = "password")
	private String password;
	
	/**
	 * Status
	 */
	@Column(name = "status")
	private boolean status;
	
	/**
	 * Address
	 */
	@Column(name = "address")
	private String address;
	
	/**
	 * Class room
	 */
	@ManyToOne
	@JoinColumn(name = "class_id", referencedColumnName="id")
	private ClassRoom class_room;
	 
	/**
	 * Get id of a student
	 * @return Id
	 */
	public long getId() {
		return id;
	}
	
	/**
	 * Get name of a student
	 * @return The name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Set name of a student
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Get email of a student
	 * @return The email
	 */
	public String getEmail() {
		return email;
	}
	
	/**
	 * Set email of a student
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	/**
	 * Get phone of a student
	 * @return The phone
	 */
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	/**
	 * Get password of a student
	 * @return password
	 */
	public String getPassword() {
		return password;
	}
	
	/**
	 * Set password of a student
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
	/**
	 * get status of a student
	 * @return
	 */
	public boolean isStatus() {
		return status;
	}
	
	/**
	 * Set status of a student
	 * @param status
	 */
	public void setStatus(boolean status) {
		this.status = status;
	}

	/**
	 * Get address of a student
	 * @return The address
	 */
	public String getAddress() {
		return address;
	}
	
	/**
	 * Set address of a student
	 * @param address
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	
	/**
	 * get class room of a student
	 * @return
	 */
	public ClassRoom getClass_room() {
		return class_room;
	}
	
	/**
	 * Set class room of a student
	 * @param class_room
	 */
	public void setClass_room(ClassRoom class_room) {
		this.class_room = class_room;
	}
}
