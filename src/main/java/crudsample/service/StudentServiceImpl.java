/**
 * Class StudentServiceImpl class provides for project.
 * 
 * @author <A HREF="mailto:phucdd@greenglobal.vn">[Phuc]</A>
 * @version $Revision: 1.0.0.0 $ $Date: 2019/05/23 $
 * @see [StudentServiceImpl]
 */
package crudsample.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import crudsample.dao.StudentDAO;
import crudsample.domain.ClassRoom;
import crudsample.domain.Student;

/**
 * The StudentServiceImpl class provides for project
 * @author DAOPHUC
 *
 */
@Service
public class StudentServiceImpl implements StudentService {
	
	/**
	 * studentDAO
	 */
	@Autowired
	private StudentDAO studentDAO;
	
	/**
	 * Get all student
	 * @return
	 */
	@Override
	@Transactional(readOnly = true)
	public List<Student> getAllStudent() {
		return studentDAO.getAllStudent();
	}
	
	/**
	 * Get student object
	 * @param id
	 * @return student
	 */
	@Override
	public Student getStudent(int id) {
		return studentDAO.getStudent(id);
	}

	/**
	 * Delete student
	 * @param id
	 * @return student
	 */
	@Override
	@Transactional
	public void deleteStudent(long id) {
		studentDAO.deleteStudent(id);
	}

	/**
	 * Save or update student
	 * @param student
	 */
	@Override
	@Transactional
	public void saveOrUpdateStudent(Student student) {
		studentDAO.saveOrUpdateStudent(student);
	}
	
	/**
	 * Get all class room
	 * @return list class room
	 */
	@Override
	@Transactional
	public List<ClassRoom> getAllClassRoom() {
		return studentDAO.getAllClassRoom();
	}
}
