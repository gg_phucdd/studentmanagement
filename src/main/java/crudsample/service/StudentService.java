/**
 * Interface StudentService class provides for project.
 * 
 * @author <A HREF="mailto:phucdd@greenglobal.vn">[Phuc]</A>
 * @version $Revision: 1.0.0.0 $ $Date: 2019/05/23 $
 * @see [StudentService]
 */
package crudsample.service;

import java.util.List;

import crudsample.domain.ClassRoom;
import crudsample.domain.Student;

/**
 * Interface StudentService class provides for project
 * @author DAOPHUC
 *
 */
public interface StudentService {

	/**
	 * Get all student
	 * @return
	 */
	public List<Student> getAllStudent();
	
	/**
	 * Get student object
	 * @param id
	 * @return student
	 */
	public Student getStudent(int id);
	
	/**
	 * Delete student
	 * @param id
	 * @return student
	 */
	public void deleteStudent(long id);
	
	/**
	 * Save or update student
	 * @param student
	 */
	public void saveOrUpdateStudent(Student student);
	
	/**
	 * Get all class room
	 * @return list class room
	 */
	public List<ClassRoom> getAllClassRoom();
}
