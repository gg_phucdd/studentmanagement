/**
 * Class ClassRoomServiceImpl class provides for project.
 * 
 * @author <A HREF="mailto:phucdd@greenglobal.vn">[Phuc]</A>
 * @version $Revision: 1.0.0.0 $ $Date: 2019/05/23 $
 * @see [ClassRoomServiceImpl]
 */
package crudsample.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import crudsample.dao.ClassRoomDAO;
import crudsample.domain.ClassRoom;

/**
 * The ClassRoomServiceImpl class provides for project
 * @author DAOPHUC
 *
 */
@Service
public class ClassRoomServiceImpl implements ClassRoomService {
	
	/**
	 * ClassRoomDAO
	 */
	@Autowired
	private ClassRoomDAO ClassRoomDAO;
	
	/**
	 * Get all ClassRoom
	 * @return
	 */
	@Override
	@Transactional(readOnly = true)
	public List<ClassRoom> getAllClassRoom() {
		return ClassRoomDAO.getAllClassRoom();
	}
	
	/**
	 * Get ClassRoom object
	 * @param id
	 * @return ClassRoom
	 */
	@Override
	public ClassRoom getClassRoom(int id) {
		return ClassRoomDAO.getClassRoom(id);
	}

	/**
	 * Delete ClassRoom
	 * @param id
	 * @return ClassRoom
	 */
	@Override
	@Transactional
	public void deleteClassRoom(long id) {
		ClassRoomDAO.deleteClassRoom(id);
	}

	/**
	 * Save or update ClassRoom
	 * @param ClassRoom
	 */
	@Override
	@Transactional
	public void saveOrUpdateClassRoom(ClassRoom ClassRoom) {
		ClassRoomDAO.saveOrUpdateClassRoom(ClassRoom);
	}
}
