/**
 * Interface ClassRoomService class provides for project.
 * 
 * @author <A HREF="mailto:phucdd@greenglobal.vn">[Phuc]</A>
 * @version $Revision: 1.0.0.0 $ $Date: 2019/05/23 $
 * @see [ClassRoomService]
 */
package crudsample.service;

import java.util.List;

import crudsample.domain.ClassRoom;

/**
 * Interface ClassRoomService class provides for project
 * @author DAOPHUC
 *
 */
public interface ClassRoomService {

	/**
	 * Get all class room
	 * @return
	 */
	public List<ClassRoom> getAllClassRoom();
	
	/**
	 * Get class room object
	 * @param id
	 * @return student
	 */
	public ClassRoom getClassRoom(int id);
	
	/**
	 * Delete class room
	 * @param id
	 */
	public void deleteClassRoom(long id);
	
	/**
	 * Save or update classRoom
	 * @param classRoom
	 */
	public void saveOrUpdateClassRoom(ClassRoom classRoom);
}
