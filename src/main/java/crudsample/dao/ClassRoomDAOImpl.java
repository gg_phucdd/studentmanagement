/**
 * Class ClassRoomDAOImpl class provides for project.
 * 
 * @author <A HREF="mailto:phucdd@greenglobal.vn">[Phuc]</A>
 * @version $Revision: 1.0.0.0 $ $Date: 2019/05/23 $
 * @see [ClassRoomDAOImpl]
 */
package crudsample.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import crudsample.domain.ClassRoom;

/**
 * The ClassRoomDAOImpl class provides for project
 * @author DAOPHUC
 *
 */
@Repository
public class ClassRoomDAOImpl implements ClassRoomDAO {

	/**
	 * session factory
	 */
	@Autowired
	SessionFactory sessionFactory;
	
	/**
	 * Get current session
	 * @return Session
	 */
	protected final Session getCurrentSession() {
	       return sessionFactory.getCurrentSession();
	}
	
	/**
	 * Get all class room
	 * @return List class room
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<ClassRoom> getAllClassRoom() {
		return getCurrentSession().createQuery("from ClassRoom").list();
	}
	
	/**
	 * Get a class room
	 * @param id
	 * @return Object class room
	 */
	@Override
	public ClassRoom getClassRoom(long id) {
		return (ClassRoom) sessionFactory.getCurrentSession().get(
				ClassRoom.class, id);
	}
	
	/**
	 * Delete a class room
	 * @param id
	 */
	@Override
	public void deleteClassRoom(long id) {
		ClassRoom classRoom = (ClassRoom) sessionFactory.getCurrentSession().load(
				ClassRoom.class, id);
		if (null != classRoom) {
			this.sessionFactory.getCurrentSession().delete(classRoom);
		}
	}
	
	/**
	 * Save or update a class room
	 * @param classRoom
	 */
	@Override
	public void saveOrUpdateClassRoom(ClassRoom classRoom) {
		sessionFactory.getCurrentSession().saveOrUpdate(classRoom);
	}
}
