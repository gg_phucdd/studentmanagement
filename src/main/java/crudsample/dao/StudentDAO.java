/**
 * Interface StudentDAO class provides for project.
 * 
 * @author <A HREF="mailto:phucdd@greenglobal.vn">[Phuc]</A>
 * @version $Revision: 1.0.0.0 $ $Date: 2019/05/23 $
 * @see [StudentDAO]
 */
package crudsample.dao;

import java.util.List;

import crudsample.domain.ClassRoom;
import crudsample.domain.Student;

/**
 * Interface StudentDAO class provides for project
 * @author DAOPHUC
 *
 */
public interface StudentDAO {
	
	/**
	 * Get all student
	 * @return List student
	 */
	public List<Student> getAllStudent();
	
	/**
	 * Get a student
	 * @param id
	 * @return Object student
	 */
	public Student getStudent(long id);
	
	/**
	 * Delete a student
	 * @param id
	 */
	public void deleteStudent(long id);
	
	/**
	 * Save or update a student
	 * @param student
	 */
	public void saveOrUpdateStudent(Student student);
	
	/**
	 * Get all class room
	 * @return List ClassRoom
	 */
	public List<ClassRoom> getAllClassRoom();
}
