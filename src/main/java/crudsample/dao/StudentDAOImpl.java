/**
 * Class StudentDAOImpl class provides for project.
 * 
 * @author <A HREF="mailto:phucdd@greenglobal.vn">[Phuc]</A>
 * @version $Revision: 1.0.0.0 $ $Date: 2019/05/23 $
 * @see [StudentDAOImpl]
 */
package crudsample.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import crudsample.domain.ClassRoom;
import crudsample.domain.Student;

/**
 * The StudentDAOImpl class provides for project
 * @author DAOPHUC
 *
 */
@Repository
public class StudentDAOImpl implements StudentDAO {

	/**
	 * session factory
	 */
	@Autowired
	SessionFactory sessionFactory;
	
	/**
	 * Get current session
	 * @return Session
	 */
	protected final Session getCurrentSession() {
	       return sessionFactory.getCurrentSession();
	}
	
	
	/**
	 * Get all student
	 * @return List student
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<Student> getAllStudent() {
		return getCurrentSession().createQuery("from Student").list();
	}
	
	/**
	 * Get all class room
	 * @return List class room
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<ClassRoom> getAllClassRoom(){
		return getCurrentSession().createQuery("from ClassRoom").list();
	}
	
	/**
	 * Get a student
	 * @param id
	 * @return Object student
	 */
	@Override
	public Student getStudent(long id) {
		return (Student) sessionFactory.getCurrentSession().get(
				Student.class, id);
	}
	
	/**
	 * Delete a student
	 * @param id
	 */
	@Override
	public void deleteStudent(long id) {
		Student student = (Student) sessionFactory.getCurrentSession().load(
				Student.class, id);
		if (null != student) {
			this.sessionFactory.getCurrentSession().delete(student);
		}
	}
	
	/**
	 * Save or update a student
	 * @param student
	 */
	@Override
	public void saveOrUpdateStudent(Student student) {
		sessionFactory.getCurrentSession().saveOrUpdate(student);
	}
}
