/**
 * Interface ClassRoomDAO class provides for project.
 * 
 * @author <A HREF="mailto:phucdd@greenglobal.vn">[Phuc]</A>
 * @version $Revision: 1.0.0.0 $ $Date: 2019/05/23 $
 * @see [ClassRoomDAO]
 */
package crudsample.dao;

import java.util.List;

import crudsample.domain.ClassRoom;

/**
 * Interface ClassRoomDAO class provides for project
 * @author DAOPHUC
 *
 */
public interface ClassRoomDAO {
	
	/**
	 * Get all class room
	 * @return List class room
	 */
	public List<ClassRoom> getAllClassRoom();
	
	/**
	 * Get a class room
	 * @param id
	 * @return Object class room
	 */
	public ClassRoom getClassRoom(long id);
	
	/**
	 * Delete a class room
	 * @param id
	 */
	public void deleteClassRoom(long id);
	
	/**
	 * Save or update a class room
	 * @param student
	 */
	public void saveOrUpdateClassRoom(ClassRoom classRoom);
}
