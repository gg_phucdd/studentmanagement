/**
 * Class ClassRoomViewModel class provides for project.
 * 
 * @author <A HREF="mailto:phucdd@greenglobal.vn">[Phuc]</A>
 * @version $Revision: 1.0.0.0 $ $Date: 2019/05/23 $
 * @see [ClassRoomViewModel]
 */
package crudsample.viewmodel;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zkplus.spring.SpringUtil;
import org.zkoss.zul.Messagebox;

import crudsample.domain.ClassRoom;
import crudsample.service.ClassRoomService;


/**
 * The ClassRoomViewModel class provides for project
 * @author DAOPHUC
 *
 */
public class ClassRoomViewModel {
	
	/**
	 * Class room service
	 */
	@Autowired
	private ClassRoomService classRoomService;
	
	/**
	 * List class room
	 */
	private List<ClassRoom> listClassRoom;
	
	/**
	 * Selected class room
	 */
	private ClassRoom selectedClassRoom;
	
	/**
	 * Get list class room
	 * @return listClassRoom
	 */
	public List<ClassRoom> getListClassRoom() {
		return listClassRoom;
	}

	/**
	 * Set list class room
	 * @param listClassRoom
	 */
	public void setListClassRoom(List<ClassRoom> listClassRoom) {
		this.listClassRoom = listClassRoom;
	}

	/**
	 * Get selected class room
	 * @return selectedClassRoom
	 */
	public ClassRoom getSelectedClassRoom() {
		return selectedClassRoom;
	}

	/**
	 * Set selected class room
	 * @param selectedClassRoom
	 */
	public void setSelectedClassRoom(ClassRoom selectedClassRoom) {
		this.selectedClassRoom = selectedClassRoom;
	}

	/**
	 * The function constructor
	 */
	@Init
	public void init() {
		 classRoomService = (ClassRoomService) SpringUtil.getBean("ClassRoomService");
		 listClassRoom = classRoomService.getAllClassRoom();
	}
	
	/**
	 * Delete this class room
	 * @param id
	 */
	@Command
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void deleteThisClassRoom(@BindingParam("myKey") final String id) {
		String str = "Would you like to delete this class room";
		Messagebox.show(str, "Confirm", Messagebox.OK | Messagebox.CANCEL,
	        Messagebox.QUESTION, new EventListener() {
	            public void onEvent(Event event) throws Exception {
	                if (((Integer) event.getData()).intValue() == Messagebox.OK) {
	                	try {
	                		classRoomService.deleteClassRoom(Integer.parseInt(id));
	                		Executions.sendRedirect("classRoom.zul");
						} catch (Exception e) {
							Messagebox.show("Failed to delete the object.", "Warning", Messagebox.OK, Messagebox.ERROR);
						}
	                }
	            }
	        });
	}
	
	/**
	 * Function Edit class room
	 * @param id
	 */
	@Command
	public void editClassRoom(@BindingParam("myKey") String id) {
		Executions.sendRedirect("/addClassRoom.zul?id=" + id + "&read=false");
	}
	
	/**
	 * Function View class room
	 * @param id
	 */
	@Command
	public void viewClassRoom(@BindingParam("myKey") String id) {
		Executions.sendRedirect("/addClassRoom.zul?id=" + id + "&read=true");
	}
	
	/**
	 * Function add class room
	 */
	@Command
	public void addClassRoom() {
		Executions.sendRedirect("/addClassRoom.zul");
	}
	
	/**
	 * Function cancel
	 */
	@Command
	public void cancel() {
		Executions.sendRedirect("/index.zul");
	}
}
