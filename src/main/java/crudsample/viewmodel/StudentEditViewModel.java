/**
 * Class StudentEditViewModel class provides for project.
 * 
 * @author <A HREF="mailto:phucdd@greenglobal.vn">[Phuc]</A>
 * @version $Revision: 1.0.0.0 $ $Date: 2019/05/23 $
 * @see [StudentEditViewModel]
 */
package crudsample.viewmodel;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zkplus.spring.SpringUtil;

import crudsample.domain.ClassRoom;
import crudsample.domain.Student;
import crudsample.service.ClassRoomService;
import crudsample.service.StudentService;

/**
 * The StudentEditViewModel class provides for project
 * @author DAOPHUC
 *
 */
public class StudentEditViewModel {
	
	/**
	 * Student service
	 */
	@Autowired
	private StudentService studentService;
	
	/**
	 * Class room service
	 */
	@Autowired
	private ClassRoomService classRoomService;;
	
	
	/**
	 * Student
	 */
	private Student student;
	
	/**
	 * Class room
	 */
	private List<ClassRoom> classRoom;
	
	/**
	 * Record mode
	 */
	private String recordMode;
	
	/**
	 * Make as read only
	 */
	private boolean makeAsReadOnly;

	/**
	 * Get student
	 * @return
	 */
	public Student getStudent() {
		return student;
	}

	/**
	 * Set student of a student
	 * @param student
	 */
	public void setStudent(Student student) {
		this.student = student;
	}

	/**
	 * Get recordMode
	 * @return recordMode
	 */
	public String getRecordMode() {
		return recordMode;
	}

	/**
	 * Set record mode of a recordMode
	 * @param recordMode
	 */
	public void setRecordMode(String recordMode) {
		this.recordMode = recordMode;
	}

	/**
	 * Is make as read only
	 * @return makeAsReadOnly
	 */
	public boolean isMakeAsReadOnly() {
		return makeAsReadOnly;
	}

	/**
	 * Set makeAsReadOnly of a makeAsReadOnly
	 * @param makeAsReadOnly
	 */
	public void setMakeAsReadOnly(boolean makeAsReadOnly) {
		this.makeAsReadOnly = makeAsReadOnly;
	}
	
	/**
	 * Get class room
	 * @return The teacher
	 */
	public List<ClassRoom> getClassRoom() {
		return classRoom;
	}

	/**
	 * Set class room
	 * @param classRoom
	 */
	public void setClassRoom(List<ClassRoom> classRoom) {
		this.classRoom = classRoom;
	}

	/**
	 * The function constructor
	 */
	@Init
	public void init() {
		studentService = (StudentService) SpringUtil.getBean("StudentService");
		classRoomService = (ClassRoomService) SpringUtil.getBean("ClassRoomService");
		Execution execution = Executions.getCurrent();
		String id = execution.getParameter("id");
		String read = execution.getParameter("read");
		classRoom = classRoomService.getAllClassRoom();
		if(id != null) {
			student = studentService.getStudent(Integer.parseInt(id));
			if(read.equals("false")) {
				this.makeAsReadOnly = false;
			}
			else {
				this.makeAsReadOnly = true;
			}
		}
		else {
			student = new Student();
			
		}
	}
	
	/**
	 * Function Save this
	 */
	@Command
	public void saveThis() {
		studentService.saveOrUpdateStudent(this.student);
		Executions.sendRedirect("index.zul");
	}
	
	/**
	 * Function cancel redirect index.zul
	 */
	@Command
	public void cancel() {
		Executions.sendRedirect("index.zul");
	}
}
