/**
 * Class ClassRoomEditViewModel class provides for project.
 * 
 * @author <A HREF="mailto:phucdd@greenglobal.vn">[Phuc]</A>
 * @version $Revision: 1.0.0.0 $ $Date: 2019/05/23 $
 * @see [ClassRoomEditViewModel]
 */
package crudsample.viewmodel;

import org.springframework.beans.factory.annotation.Autowired;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zkplus.spring.SpringUtil;

import crudsample.domain.ClassRoom;
import crudsample.service.ClassRoomService;

/**
 * The ClassRoomEditViewModel class provides for project
 * @author DAOPHUC
 *
 */
public class ClassRoomEditViewModel {	
	/**
	 * Class room service
	 */
	@Autowired
	private ClassRoomService classRoomService;;
	
	
	/**
	 * ClassRoom
	 */
	private ClassRoom classRoom;
	
	/**
	 * Record mode
	 */
	private String recordMode;
	
	/**
	 * Make as read only
	 */
	private boolean makeAsReadOnly;

	/**
	 * Get ClassRoom
	 * @return
	 */
	public ClassRoom getClassRoom() {
		return classRoom;
	}

	/**
	 * Set ClassRoom of a ClassRoom
	 * @param ClassRoom
	 */
	public void setClassRoom(ClassRoom classRoom) {
		this.classRoom = classRoom;
	}

	/**
	 * Get recordMode
	 * @return recordMode
	 */
	public String getRecordMode() {
		return recordMode;
	}

	/**
	 * Set record mode of a recordMode
	 * @param recordMode
	 */
	public void setRecordMode(String recordMode) {
		this.recordMode = recordMode;
	}

	/**
	 * Is make as read only
	 * @return makeAsReadOnly
	 */
	public boolean isMakeAsReadOnly() {
		return makeAsReadOnly;
	}

	/**
	 * Set makeAsReadOnly of a makeAsReadOnly
	 * @param makeAsReadOnly
	 */
	public void setMakeAsReadOnly(boolean makeAsReadOnly) {
		this.makeAsReadOnly = makeAsReadOnly;
	}

	/**
	 * The function constructor
	 */
	@Init
	public void init() {
		classRoomService = (ClassRoomService) SpringUtil.getBean("ClassRoomService");
		Execution execution = Executions.getCurrent();
		String id = execution.getParameter("id");
		String read = execution.getParameter("read");
		if(id != null) {
			classRoom = classRoomService.getClassRoom(Integer.parseInt(id));
			if(read.equals("false")) {
				this.makeAsReadOnly = false;
			}
			else {
				this.makeAsReadOnly = true;
			}
		}
		else {
			classRoom = new ClassRoom();
		}
	}
	
	/**
	 * Function Save this
	 */
	@Command
	public void saveThis() {
		classRoomService.saveOrUpdateClassRoom(this.classRoom);
		Executions.sendRedirect("classRoom.zul");
	}
	
	/**
	 * Function cancel redirect classRoom.zul
	 */
	@Command
	public void cancel() {
		Executions.sendRedirect("classRoom.zul");
	}
}
