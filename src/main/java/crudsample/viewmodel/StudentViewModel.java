/**
 * Class StudentViewModel class provides for project.
 * 
 * @author <A HREF="mailto:phucdd@greenglobal.vn">[Phuc]</A>
 * @version $Revision: 1.0.0.0 $ $Date: 2019/05/23 $
 * @see [StudentViewModel]
 */
package crudsample.viewmodel;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zkplus.spring.SpringUtil;
import org.zkoss.zul.Messagebox;

import crudsample.domain.Student;
import crudsample.service.StudentService;

/**
 * The StudentViewModel class provides for project
 * @author DAOPHUC
 *
 */
public class StudentViewModel {
	
	/**
	 * Student service
	 */
	@Autowired
	private StudentService studentService;
	
	/**
	 * List student
	 */
	private List<Student> listStudent;
	
	/**
	 * Selected student
	 */
	private Student selectedStudent;
	
	/**
	 * Get list student
	 * @return listStudent
	 */
	public List<Student> getListStudent() {
		return listStudent;
	}

	/**
	 * Set list student
	 * @param listStudent
	 */
	public void setListStudent(List<Student> listStudent) {
		this.listStudent = listStudent;
	}

	/**
	 * Get selected student
	 * @return selectedStudent
	 */
	public Student getSelectedStudent() {
		return selectedStudent;
	}

	public void setSelectedStudent(Student selectedStudent) {
		this.selectedStudent = selectedStudent;
	}

	/**
	 * The function constructor
	 */
	@Init
	public void init() {
		 studentService = (StudentService) SpringUtil.getBean("StudentService");
		 listStudent = studentService.getAllStudent();
	}
	
	/**
	 * Delete this student
	 * @param id
	 */
	@Command
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void deleteThisStudent(@BindingParam("myKey") final String id) {
		String str = "Would you like to delete this student";
		Messagebox.show(str, "Confirm", Messagebox.OK | Messagebox.CANCEL,
	        Messagebox.QUESTION, new EventListener() {
	            public void onEvent(Event event) throws Exception {
	                if (((Integer) event.getData()).intValue() == Messagebox.OK) {
	              	    studentService.deleteStudent(Integer.parseInt(id));
	              	    Executions.sendRedirect("index.zul");
	                }
	            }
	        });
	}
	
	/**
	 * Function Edit Student
	 * @param id
	 */
	@Command
	public void editStudent(@BindingParam("myKey") String id) {
		Executions.sendRedirect("/addStudent.zul?id=" + id + "&read=false");
	}
	
	/**
	 * Function View student
	 * @param id
	 */
	@Command
	public void viewStudent(@BindingParam("myKey") String id) {
		Executions.sendRedirect("/editStudent.zul?id=" + id + "&read=true");
	}
	
	/**
	 * Function add Student
	 */
	@Command
	public void addStudent() {
		Executions.sendRedirect("/addStudent.zul");
	}
	
	/**
	 * Function redirect class room
	 */
	@Command
	public void redirectClassRoom() {
		Executions.sendRedirect("/classRoom.zul");
	}
}
